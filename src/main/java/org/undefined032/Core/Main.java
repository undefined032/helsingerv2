package org.undefined032.Core;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.activity.ActivityType;
import org.javacord.api.entity.user.UserStatus;
import org.undefined032.SlashCommands.StatsSlashCommand;
import org.undefined032.SlashCommands.GuildInfoSlashCommand;

import org.javacord.api.interaction.SlashCommand;

import org.undefined032.Utils.Privates;


public class Main {
    public static void main(String[] args) {
        DiscordApi javacord = new DiscordApiBuilder().setToken(Privates.apiToken).login().join();
        javacord.addSlashCommandCreateListener(new StatsSlashCommand());
        javacord.addSlashCommandCreateListener(new GuildInfoSlashCommand());


        javacord.addServerJoinListener(event -> {
           SlashCommand.with("stats", "Просмотр статистики бота")
                    .createForServer(event.getServer())
                    .join();
            SlashCommand.with("guildinfo", "Информация о сервере")
                .createForServer(event.getServer())
                .join();
        });
        javacord.updateActivity(ActivityType.LISTENING, "DiscordAPI");
        javacord.updateStatus(UserStatus.IDLE);
        System.out.println("Бот запустился, кол-во серверов " + javacord.getServers().toArray().length);
    }


}
