package org.undefined032.SlashCommands;

import org.javacord.api.DiscordApi;
import org.javacord.api.entity.message.component.ActionRow;
import org.javacord.api.entity.message.component.Button;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.event.interaction.SlashCommandCreateEvent;
import org.javacord.api.interaction.*;
import org.javacord.api.listener.interaction.SlashCommandCreateListener;

import java.awt.*;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.concurrent.TimeUnit;

public class StatsSlashCommand implements SlashCommandCreateListener {
    @Override
    public void onSlashCommandCreate(SlashCommandCreateEvent event) {
        SlashCommandInteraction interaction = event.getSlashCommandInteraction();
        DiscordApi javacord = interaction.getApi();
        if(interaction.getCommandName().equals("stats")) {
            RuntimeMXBean rb = ManagementFactory.getRuntimeMXBean();
            EmbedBuilder embed = new EmbedBuilder()
                    .setAuthor("Информация о боте")
                    .setDescription("Разработчик: **undefined032#6666**\nХостинг: **Orange Pi PC**\nЛокация: :flag_ee: **Эстония**\nБиблиотека: **Javacord v3.3.2**")
                    .addField("Статистика",
                            "```Серверов: " + javacord.getServers().toArray().length +
                                    "\nПользователей в кеше: " + javacord.getCachedUsers().toArray().length + "" +
                                    "\nПинг: " + javacord.getLatestGatewayLatency().toMillis() +
                                    "\nАптайм: " + TimeUnit.MILLISECONDS.toMinutes(rb.getUptime()) + " минут, " + TimeUnit.MILLISECONDS.toHours(rb.getUptime()) + " часов, " + TimeUnit.MILLISECONDS.toDays(rb.getUptime()) + " дней```", false)
                    .setTimestampToNow()
                    .setColor(Color.decode("#A09AE7"));
            interaction.createImmediateResponder()
                    .addEmbed(embed)
                    .addComponents(
                            ActionRow.of(
                                    Button.link("https://discord.com/api/oauth2/authorize?client_id=861300261359845396&permissions=268823622&scope=bot", "Пригласить бота"),
                                    Button.link("https://discord.gg/XarjMSnKj9", "Сервер поддержки")

                            )
                    )
                    .respond();
        } else return;
    }
}