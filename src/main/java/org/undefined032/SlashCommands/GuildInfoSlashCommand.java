package org.undefined032.SlashCommands;

import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.event.interaction.SlashCommandCreateEvent;
import org.javacord.api.interaction.*;
import org.javacord.api.listener.interaction.SlashCommandCreateListener;

import java.awt.*;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class GuildInfoSlashCommand implements SlashCommandCreateListener {
    @Override
    public void onSlashCommandCreate(SlashCommandCreateEvent event) {
        SlashCommandInteraction interaction = event.getSlashCommandInteraction();
        DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy")
                .withZone(ZoneId.systemDefault());
        if(interaction.getCommandName().equals("guildinfo")) {
            Server guild = interaction.getServer().get();
            EmbedBuilder embed = new EmbedBuilder()
                    .setTitle(guild.getName())
                    .setDescription("Владелец: <@" + guild.getOwnerId() + ">"
                            + "\nРолей: **" + guild.getRoles().toArray().length + "**"
                            + "\nДата создания: **" + DATE_TIME_FORMATTER.format(guild.getCreationTimestamp()) + "**"
                            + "\nУчастников: **" + guild.getMemberCount() + "**"
                    )
                    .setThumbnail(guild.getIcon().get().getUrl().toString())
                    .setTimestampToNow()
                    .setColor(Color.decode("#A09AE7"));
            interaction.createImmediateResponder()
                    .addEmbed(embed)
                    .respond();
        } else return;
    }
}